package main.java;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;
import java.util.List;

public class Main {
    public static void main(String[] args) {

        // Selenium init.
        WebDriverManager.chromedriver().setup();
        ChromeOptions options = new ChromeOptions();
        options.addArguments("--remote-allow-origins=*");
        WebDriver driver = new ChromeDriver(options);
        driver.manage().window().maximize();

        // Variables for the upcoming dog video search.
        boolean dogSearch = true;
        byte refreshCount = 0;

        // Dog video search.
        driver.get("https://www.youtube.com");

        WebElement acceptCookie = driver.findElement(
                By.xpath("//button[contains(@aria-label, 'Accept')]"));
        ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);",
                acceptCookie); acceptCookie.click();

        while (dogSearch & (refreshCount < 10)) {
            new WebDriverWait(driver, Duration.ofSeconds(5))
                    .until(ExpectedConditions.visibilityOfElementLocated(
                            By.xpath("//h3/a")));
            List<WebElement> titleList = driver.findElements(By.xpath("//h3/a"));
            for (WebElement video : titleList) {
                if (video.getText().contains("Summer")) {
                    video.click();
                    dogSearch = false;
                    break;
                }
            }
            driver.navigate().refresh();
            refreshCount++;
        }
        if (refreshCount == 10) driver.quit();

        // Making a search request with a video name.
        new WebDriverWait(driver, Duration.ofSeconds(10))
                .until(ExpectedConditions.visibilityOfElementLocated(
                        By.xpath("//div[@id='title']/h1[contains(.,'Summer')]")));
        WebElement videoName = driver.findElement(
                By.xpath("//div[@id='title']/h1[contains(.,'Summer')]"));
        WebElement searchQuery = driver.findElement(By.name("search_query"));
        searchQuery.sendKeys(videoName.getText());
        searchQuery.sendKeys(Keys.ENTER);

        // Variables for playlist link search.
        boolean playlistFound = false;
        byte scrollCount = 0;

        // Find a playlist by scrolling. When found -> click.
        while (!playlistFound & (scrollCount < 20)){
            try{
                WebElement playlist = driver.findElement(By.cssSelector("yt-formatted-string#view-more>a"));
                playlist.click();
                playlistFound = true;
            }
            catch(NoSuchElementException e){
                JavascriptExecutor js = (JavascriptExecutor) driver;
                js.executeScript("window.scrollBy(0,250)");
                scrollCount++;
                e.printStackTrace();
            }
        }

        // Getting to ICTV YT channel.
        new WebDriverWait(driver, Duration.ofSeconds(10)).until(ExpectedConditions.elementToBeClickable(By.xpath("(//a[@href='/c/SerialyICTV'])[2]")));
        WebElement ictvLink = driver.findElement(By.xpath("(//a[@href='/c/SerialyICTV'])[2]"));
        ictvLink.click();
    }
}